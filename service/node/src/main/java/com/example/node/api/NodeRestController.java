package com.example.node.api;


import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

@RestController
public class NodeRestController {

    @RequestMapping("hello")
    public Map<String, String> getHello() {
        Map<String, String> data = new HashMap<>();
        data.put("first", "Hello-Node");
        data.put("second", "goodbye-Node");
        return data;
    }

    @KafkaListener(topics = "test-kafka", groupId = "group_id")
    public void getHelloMessage(String message) {
        System.out.println("Message: " + message);
    }

    @RequestMapping("hellos")
    public Map<String, String> getAllHello() {
        RestTemplate restTemplate = new RestTemplate();
        return restTemplate.getForObject("http://localhost:9000/user/hello", Map.class);
    }

}
