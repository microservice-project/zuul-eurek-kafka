package com.example.user.api;


import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
public class UserRestController {

    @Autowired
    KafkaProducer<String, String> producer;

    int i = 1;
    Map<String, String> data;

    public UserRestController() {
        data = new HashMap<>();
        data.put("first", "Hello-User");
        data.put("second", "goodbye-User");
    }

    @GetMapping("hello")
    public Map<String, String> getHello() {
        return data;
    }

    @GetMapping("hello/{value}")
    public Map<String, String> postHello(@PathVariable("value") String value) {
        data.put("key-" + i++, value);

        String topic = "test-kafka";
        ProducerRecord<String, String> producerRecord = new ProducerRecord<>(topic, value);
        producer.send(producerRecord);

        return data;
    }

}
